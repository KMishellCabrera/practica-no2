package practicano2;

import java.util.Scanner;

//@author Mishell Cabrera

public class Ejercicio5umg {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        int x = 0;
        int ecuacion;

        System.out.println("f(x,y) = x + 3y * x^2");
        System.out.println("INGRESE EL VALOR DE X");
        x = entrada.nextInt();

        ecuacion = x * x * 3;
        System.out.println(ecuacion + "y" + " + " + x);
    }

}
